# API DOCS

## CEO

### Daily Stats

Endpoint: `ceo/daily_stats`

Method: GET

Request: None

Response:

Success:

```json
{
    "status": "success",
    "message": "Stats information retrieved successfully",
    "order": {
        "delivered": 123,
        "in_progress": 123,
        "estimated": 123
    }
}
```

Fail:

```json
{
    "status": "fail",
    "message": "{Error description}"
}
```

### Map

Endpoint: `/ceo/map`

Method: GET

Request: None

Response:

Success:

```json
{
    "status": "success",
    "message": "List of orders retrieved successfully",
    "drivers": [
        {
            "id": 123,
            "name": "John Doe",
            "lat": "12.345678",
            "lng": "12.345678"
        },
    ]
}
```

Fail:

```json
{
    "status": "fail",
    "message": "{Error description}"
}
```

### Share Price - NOT IMPLEMENTED YET

Endpoint: `/ceo/share_price`

Method: GET

Request: None

Response:

```json
{
    "status": "success",
    "message": "30.54 EUR"
}
```

## Customer

### Register

Endpoint: `/customer/register`

Method: POST

Request:

```json
{
    "name": "John Doe",
    "email": "john.doe@example.com",
    "phone": "+79876543210",
    "password": "p@ssw0rd"
}
```

Response:

Success:

```json
{
    "status": "success",
    "message": "Client successfully registered"
}
```

Fail:

```json
{
    "status": "fail",
    "message": "{Error description}"
}
```

### Login

Endpoint: `/customer/login`

Method: POST

Request:

```json
{
    "email": "john.doe@example.com",
    "password": "p@ssw0rd"
}
```

Response:

Success:

```json
{
    "status": "success",
    "message": "Logged in as John Doe",
    "access_token": "JWT-token"
}
```

Fail:

```json
{
    "status": "fail",
    "message": "{Error description}"
}
```

### Create Order

Endpoint: `/customer/create_order`

Method: POST

Request:

```json
{
    "delivery_address": "Innopolis, Universitetskaya, st. 1/2 room 234",
    "pickup_address": "Innopolis, Universitetskaya, st. 1/4 room 316",
    "time_windows": "07:00-11:00,19:00-23:00",
    "delivery_coordinates": {
        "latitude": "12.345678",
        "longitude": "12.345678"
    }
}
```

Response:

```json
{
    "status": "success / fail",
    "message": "Order created successfully / {Error description}"
}
```

### Order List

Endpoint: `/customer/order_list`

Method: GET

Request: None

Response:

Success:

```json
{
    "status": "success",
    "message": "List of orders retrieved successfully",
    "orders": [
        {
            "parcel_id": 123,
            "status": "STATUS"
        },
    ]
}
```

Fail:

```json
{
    "status": "fail",
    "message": "{Error description}"
}
```

### Order Info

Endpoint: `/customer/order_info`

Method: GET

Request: `?parcel_id=123`

Response:

Success:

```json
{
    "status": "success",
    "message": "Information about the order 123 retrieved successfully",
    "order": {
        "parcel_id": 123,
        "delivery_address": "Innopolis, Universitetskaya, st. 1/2 room 234",
        "pickup_address": "Innopolis, Universitetskaya, st. 1/4 room 316",
        "delivery_coordinates": {
            "latitude": "12.345678",
            "longitude": "12.345678"
        },
        "status": "STATUS"
    }
}
```

Fail:

```json
{
    "status": "fail",
    "message": "{Error description}"
}
```

## Driver

### Login

Endpoint: `/driver/login`

Method: POST

Request:

```json
{
    "login": "driverPetya",
    "password": "p@ssw0rd"
}
```

Response:

Success:

```json
{
    "status": "success",
    "message": "Logged in as John Doe",
    "access_token": "JWT-token"
}
```

Fail:

```json
{
    "status": "fail",
    "message": "{Error description}"
}
```

### Map

Endpoint: `/driver/map`

Method: POST

Request:

```json
{
    "latitude": "12.345678",
    "longitude": "12.345678"
}
```

Response:

Success:

```json
{
    "status": "success",
    "message": "Coordinates updated"
}
```

Fail:

```json
{
    "status": "fail",
    "message": "{Error description}"
}
```

### Current deliveries list

Endpoint: `/driver/current_delivery`

Method: GET

Request: None (valid after login)

Response:

Success (some info exists):

```json
{
        "delivery_id": 1,
        "delivery_address": "Innopolis, Universitetskaya, st. 1/2 room 234" ,
        "pickup_address": "Innopolis, Universitetskaya, st. 1/2 room 234",
        "delivery_coordinates": {
            "latitude": "12.345678",
            "longitude": "12.345678"
        },
        "time_windows": "19:00-20:00",
        "client_name": "Maria",
        "telephone": "+1234567890",
        "email": "maria@gmail.com",
        "status": "IN_PROGRESS"
    }

```
Success (no current_deliveries):
```json
{
    "success",
    "No current deliveries"
}

Fail:

```json
{
    "status": "fail",
    "message": "{Error description}"
}
```

### Future deliveries list

Endpoint: `/driver/future_deliveries`

Method: GET

Request: None (valid after login)

Response:

Success (some info exists):

```json
{
       "delivery_id": 111,
        "delivery_address": some address,
        "client_name": Petya,
        "useful_data": report,
        "status": TO_DELIVER,
        "delivery_coordinates":
            {
                "latitude": 12.3,
                "longitude": 55.3
            }


    }

```

Success (no future_deliveries):
```json
{
    "success",
    "No more deliveries"
}

Fail:

```json
{
    "status": "fail",
    "message": "{Error description}"
}
```

### Update delivery status

Endpoint: `/driver/update_status`

Method: GET

Request:

```json
{
    "parcel_id": 123,
    "status": "Success"
}
```

```json
{
    "parcel_id": 123,
    "status": "Fail",
    "report": "Some text"
}
```

Response:

Success:

```json
{
    "status": "success",
    "message": "Driver's delivery status successfully updated"
}
```

Fail:

```json
{
    "status": "fail",
    "message": "{Error description}"
}
```

### Passed deliveries list

Endpoint: `/driver/passed_deliveries`

Method: GET

Request: None (valid after login)

Response:

Success (some info exists):

```json
[ {
        "delivery_id": 1,
        "delivery_address": "Innopolis, Universitetskaya, st. 1/2 room 234"  ,
        "client_name": "Maria",
        "useful_data": "End time: 20:45, feedback:good",
        "status": "SUCCESS"
    } ]


```
Success (no passed_deliveries):
```json
{
    "success",
    "No previous deliveries"
}

Fail:

```json
{
    "status": "fail",
    "message": "{Error description}"
}
```

## Operator

### Register driver

Endpoint: `/operator/register`

Method: POST

Request:

```json
{
    "name": "John Doe",
    "login": "john.doe",
    "phone": "+79876543210",
    "password": "p@ssw0rd"
}
```

Response:

Success:

```json
{
    "status": "success",
    "message": "Driver successfully registered"
}
```

Fail:

```json
{
    "status": "fail",
    "message": "{Error description}"
}
```

### List Deliveries

Endpoint: `/operator/list_deliveries`

Method: GET

Request: None

Response:

Success:

```json
{
    "status": "success",
    "message": "List of deliveries retrieved successfully",
    "deliveries": [
        {
            "parcel_id": 123,
            "driver_id": 123,
            "delivery_address": "Innopolis, Universitetskaya, st. 1/2 room 234",
            "pickup_address": "Innopolis, Universitetskaya, st. 1/4 room 316",
            "delivery_coordinates": {
                "latitude": "12.345678",
                "longitude": "12.345678"
            },
            "pickup_coordinates":
            {
                "latitude": "12.345678",
                "longitude": "12.345678"
            },
            "status": "STATUS"
        },
    ]
}
```

Fail:

```json
{
    "status": "fail",
    "message": "{Error description}"
}
```

### Map

Endpoint: `/operator/map`

Method: GET

Request: None

Response:

Success:

```json
{
    "status": "success",
    "message": "List of orders retrieved successfully",
    "drivers": [
        {
            "driver_id": 123,
            "name": "John Doe",
            "latitude": "12.345678",
            "longitude": "12.345678"
        },
    ]
}
```

Fail:

```json
{
    "status": "fail",
    "message": "{Error description}"
}
```

### Assign Driver

Endpoint: `/operator/assign_driver`

Method: POST

Request:

```json
{
    "driver_id": 123,
    "parcel_id": 123
}
```

Response:

Success:

```json
{
    "status": "success",
    "message": "Parcel with ID 123 assigned to driver with ID 123"
}
```

Fail:

```json
{
    "status": "fail",
    "message": "{Error description}"
}
```